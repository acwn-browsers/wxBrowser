/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/


// https://stackoverflow.com/questions/15836253/c-populate-vector-from-sqlite3-callback-function
// https://stackoverflow.com/questions/16942109/sqlite-replace-without-auto-increment

#include "wxbbookmarks.h"

#include <iostream>
#include <stdexcept>

#include <cstdio>

#include <boost/lexical_cast.hpp>

WxbBookmarks::WxbBookmarks (void)
{
	int result;
	std::string create1 = "CREATE TABLE IF NOT EXISTS favorites (id INTEGER PRIMARY KEY ASC NOT NULL, name TEXT UNIQUE ON CONFLICT ROLLBACK, address TEXT, cat_id INTEGER DEFAULT 0, description TEXT);";
	std::string create2 = "CREATE TABLE IF NOT EXISTS categories (id INTEGER PRIMARY KEY ASC NOT NULL, name TEXT UNIQUE ON CONFLICT ROLLBACK, description TEXT, parent_id INTEGER DEFAULT 0, sequence INTEGER DEFAULT 0);";

	wxString f_dir;
	wxString f_file;

	f_dir = wxGetHomeDir ();
	f_dir.Append (wxT ("/.wxBrowser/"));
	if (!wxDir ().Exists (f_dir))
	{
		wxMkdir (f_dir, 0770);
	}
	f_file.Append (f_dir).Append (wxT ("bookmarks.sqlite"));

	result = sqlite3_open (f_file.c_str (), &m_dbconnection);
	if (result != SQLITE_OK)
	{
		sqlite3_close (m_dbconnection);
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): Could not open the database!");
	}
	else
	{
		Execute (create1);
		Execute (create2);
		SaveCategory ("wxBrowser", "Bookmarks to wxBrowser project");
	}
}

WxbBookmarks::~WxbBookmarks ()
{
	sqlite3_close (m_dbconnection);
}

bool WxbBookmarks::Execute (wxString query)
{
	int rc;
	char* emsg = 0;

	rc = sqlite3_exec (m_dbconnection, query.c_str(), NULL, NULL, &emsg);

	if (rc != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + emsg + " with query: " + query.ToStdString ());
	}
}

bool WxbBookmarks::Execute (wxString query, std::vector <wxString> m_CategoryList)
{
	int rc;
	char* emsg = 0;

	rc = sqlite3_exec (m_dbconnection, query.c_str(), WxbBookmarks::Callback, &m_CategoryList, &emsg);
	if (rc != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + emsg);
	}
}

int WxbBookmarks::Callback (void *pointer, int argc, char **argv, char **azColName)
{
	for (int i = 0; i < argc; ++i)
	{
		
	}
	return 0;
}

wxMenu* WxbBookmarks::GetMenu (void)
{
	wxArrayString f_submenunames;
	std::vector <struct s_FavoriteRow> f_favorites;

	wxMenu* menu = new wxMenu();
	menu->Append (200, "Manage bookmarks");
	menu->AppendSeparator();
	
	f_submenunames = GetAllCategories ();
	wxMenu *submenus[f_submenunames.GetCount()];
	
	for (int i=0; i < f_submenunames.GetCount(); ++i)
	{
		submenus[i] = new wxMenu ();
		menu->AppendSubMenu (submenus[i], f_submenunames[i]);
		f_favorites = GetFavoritesByCategory (f_submenunames[i]);
		for (int j= 0; j < f_favorites.size(); ++j)
		{
			submenus[i]->Append (0, f_favorites[j].name, f_favorites[j].address);
		}
	}
	return menu;
}

void WxbBookmarks::SaveFavorite (wxString name, wxString address, wxString category, wxString description)
{
	wxString f_query01;
	int cat_id;
	
	cat_id = GetCategoryID (category);
	f_query01 = "INSERT INTO favorites (name, address, cat_id, description) VALUES (\"" + name + "\", \"" + address + "\", " + boost::lexical_cast<std::string>(cat_id) + " ,\"" + description + "\");";
	Execute (f_query01);
	SaveCategory (category);
}

void WxbBookmarks::DeleteFavorite (wxString name)
{
	wxString f_query01 = "DELETE FROM favorites WHERE name = \"" + name + "\";";
	
	Execute (f_query01);
}

std::vector <wxArrayString> WxbBookmarks::GetAllFavorites (wxString category)
{
	int f_result01;
}

std::vector <struct s_FavoriteRow> WxbBookmarks::GetFavoritesByCategory (wxString category)
{
	int f_result;
	sqlite3_stmt *f_stmt;
	wxString f_query01 = "SELECT favorites.id,favorites.name,favorites.address,favorites.description FROM favorites LEFT JOIN categories ON favorites.cat_id=categories.id WHERE categories.name=\"" + category + "\";";
	std::vector <struct s_FavoriteRow> f_favorites;
	s_FavoriteRow *f_fav_row;

	f_result = sqlite3_prepare_v2 (m_dbconnection, f_query01, -1, &f_stmt, NULL);
	if (f_result != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}
	while (sqlite3_step (f_stmt) == SQLITE_ROW)
	{
		f_fav_row = new s_FavoriteRow;
		f_fav_row->id = sqlite3_column_int (f_stmt, 0);
		f_fav_row->name = sqlite3_column_text (f_stmt, 1);
		f_fav_row->address = sqlite3_column_text (f_stmt, 2);
		f_fav_row->description = sqlite3_column_text (f_stmt, 3);
		f_favorites.push_back (*f_fav_row);
		delete f_fav_row;
	}

	if (sqlite3_finalize (f_stmt) != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}
	return f_favorites;
}

wxString WxbBookmarks::GetAddress (int ID)
{
	int f_result01;
	sqlite3_stmt *f_stmt01;
	wxString f_retval;
	wxString f_ID = wxString::Format(_T("%d"), ID);
	wxString f_query01 = "SELECT address FROM favorites WHERE id=\"" + f_ID + "\" LIMIT 1;";
	
	f_result01 = sqlite3_prepare_v2 (m_dbconnection, f_query01, -1, &f_stmt01, NULL);
	if (f_result01 != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}
	while (sqlite3_step (f_stmt01) == SQLITE_ROW)
	{
		f_retval = sqlite3_column_text (f_stmt01, 0);
	}

	if (sqlite3_finalize (f_stmt01) != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}
	return f_retval;
}

void WxbBookmarks::SaveCategory (wxString category, wxString description, int sequence)
{
	int f_result01;
	int f_result02;
	wxString f_query01 = "SELECT name from categories WHERE name =\"" + category + "\";";
	wxString f_query02 = "INSERT INTO categories (name) VALUES (\"" + category + "\");";
	sqlite3_stmt *f_stmt01;
	sqlite3_stmt *f_stmt02;

	f_result01 = sqlite3_prepare_v2 (m_dbconnection, f_query01, -1, &f_stmt01, NULL);
	if (f_result01 != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}

	f_result02 = sqlite3_prepare_v2 (m_dbconnection, f_query02, -1, &f_stmt02, NULL);
	if (f_result02 != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}

	if (sqlite3_step (f_stmt01) == SQLITE_ROW)
	{
		sqlite3_finalize (f_stmt01);
		sqlite3_finalize (f_stmt02);
		return;
	}
	else
	{
		Execute (f_query02);
		sqlite3_finalize (f_stmt01);
		sqlite3_finalize (f_stmt02);
	}
}

void WxbBookmarks::DeleteCategory (wxString category)
{
	wxString f_query01 = "DELETE FROM categories WHERE name=\"" + category + "\";";
	Execute (f_query01);
}

wxArrayString WxbBookmarks::GetAllCategories (void)
{
	int f_result;
	sqlite3_stmt *f_stmt;
	wxArrayString f_categories;

	f_result = sqlite3_prepare_v2 (m_dbconnection, "SELECT name FROM categories WHERE id != 0 ORDER BY sequence ASC", -1, &f_stmt, NULL);
	if (f_result != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}
	while (sqlite3_step (f_stmt) == SQLITE_ROW)
	{
		f_categories.Add (sqlite3_column_text (f_stmt, 0));
	}

	if (sqlite3_finalize (f_stmt) != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}

	return f_categories;
}

int WxbBookmarks::GetCategoryID (wxString category)
{
	int f_catid;
	int f_result;
	sqlite3_stmt *f_stmt;
	wxString f_query = "SELECT id FROM categories WHERE name = \"" + category + "\" LIMIT 1;";
	
	f_result = sqlite3_prepare_v2 (m_dbconnection, f_query, -1, &f_stmt, NULL);
	if (f_result != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}
	while (sqlite3_step (f_stmt) == SQLITE_ROW)
	{
		f_catid = sqlite3_column_int (f_stmt, 0);
	}
	if (sqlite3_finalize (f_stmt) != SQLITE_OK)
	{
		char buf[5];
		sprintf (buf, "%d", __LINE__);
		throw std::runtime_error (std::string ("SQL Error (#") + buf + "): " + sqlite3_errmsg (m_dbconnection));
	}
	return f_catid;
}
