/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBMANAGEBOOKMARKDIALOG__
#define __WXBMANAGEBOOKMARKDIALOG__

#include <wx/wx.h>
#include <wx/treectrl.h>
#include <wx/listctrl.h>

#include "wxbbookmarks.h"

class WxbManageBookmarkDialog : public wxDialog
{
	private:
		enum
		{
			wxB_TREECTRL = 2001,
			wxB_CATEGORY_ADD,
			wxB_CATEGORY_DELETE,
			wxB_CATEGORY_UP,
			wxB_CATEGORY_DOWN,
			wxB_BOOKMARK_EDIT,
			wxB_BOOKMARK_DELETE,
			wxB_LISTCTRL
		};

		enum e_ui_state
		{
			STATE_NORMAL,
			STATE_ROOT,
			STATE_LISTITEM_SELECTED,
			STATE_LISTITEM_UNSELECTED
		};

		wxTreeCtrl* m_treectrl;
		wxListCtrl* m_listctrl;
		wxTreeItemId m_rootid;

		wxBitmapButton* m_categoryadd_button;
		wxBitmapButton* m_categorydelete_button;
		wxBitmapButton* m_categoryup_button;
		wxBitmapButton* m_categorydown_button;
		wxButton* m_bookmarkedit_button;
		wxBitmapButton* m_bookmarkdelete_button;

		wxPanel* CreateLeftPanel (wxWindow* parent);
		wxPanel* CreateRightPanel (wxWindow* parent);
		void UpdateUI (e_ui_state state);

		WxbBookmarks *marks;

		/* Callbacks */
		void OnTreeCtrlChanged (wxTreeEvent& event);
		void OnTreeCtrlActivated (wxTreeEvent& event);

		void OnButtonCategoryAdd (wxCommandEvent& event);
		void OnButtonCategoryDelete (wxCommandEvent& event);
		void OnButtonCategoryUp (wxCommandEvent& event);
		void OnButtonCategoryDown (wxCommandEvent& event);
		void OnButtonBookmarkEdit (wxCommandEvent& event);
		void OnButtonBookmarkDelete (wxCommandEvent& event);

		void OnListCtrlItemSelected (wxListEvent& event);
		
		void AddRow (wxString Name, wxString Address, wxString Description);

	public:
		WxbManageBookmarkDialog (wxWindow* parent, const wxString& title);
		~WxbManageBookmarkDialog ();
};

#endif
