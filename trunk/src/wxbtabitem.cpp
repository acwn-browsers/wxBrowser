/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>

#include <wx/artprov.h>
#include <wx/regex.h>
#include <wx/bookctrl.h>

#include "wxbtabitem.h"
#include "wxbtabbook.h"
#include "wxbbookmarkdialog.h"
#include "wxbevents.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

WxbTabitem::WxbTabitem (wxWindow* parent, wxString url, wxString startpage) : wxPanel (parent), m_url (url), m_startpage (startpage)
{
	wxString iconfile (DATADIR);

	wxBoxSizer *vbox1 = new wxBoxSizer  (wxVERTICAL);
	wxFlexGridSizer *fgs = new wxFlexGridSizer (3,4,0,0);

	/* first row */
	wxBoxSizer *hbox1 = new wxBoxSizer (wxHORIZONTAL);
	wxBitmapButton* backbutton = new wxBitmapButton (this, wxbID_BACKBUTTON, wxArtProvider::GetBitmap (wxART_GO_BACK));
	backbutton->SetToolTip ("Go back");

	wxBitmapButton* forwardbutton = new wxBitmapButton (this, wxbID_FORWARDBUTTON, wxArtProvider::GetBitmap (wxART_GO_FORWARD));
	forwardbutton->SetToolTip ("Go forward");

	reloadbutton = new wxBitmapButton (this, wxbID_RELOADBUTTON, wxArtProvider::GetBitmap (wxART_CROSS_MARK));
	reloadbutton->SetToolTip ("Reload page");

	wxBitmapButton* homebutton = new wxBitmapButton (this, wxbID_HOMEBUTTON, wxArtProvider::GetBitmap (wxART_GO_HOME));
	homebutton->SetToolTip ("Load startpage");

	urltext = new wxTextCtrl (this, wxbID_TEXTCTRL, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);

	iconfile.Append ("/icons/22x22/bookmark-new.png");
	wxBitmap bitmap1 (iconfile);
	wxBitmapButton* addbookmarkbutton = new wxBitmapButton (this, wxbID_ADD_BOOKMARK, bitmap1);
	addbookmarkbutton->SetToolTip ("Add bookmark");

	iconfile.clear ();
	iconfile.Append (DATADIR);
	iconfile.Append ("/icons/22x22/view-zoom-in.png");
	wxBitmap bitmap2 (iconfile);
	wxBitmapButton* zoomin = new wxBitmapButton (this, wxbID_ZOOM_IN, bitmap2);
	zoomin->SetToolTip ("Zoom in");

	iconfile.clear ();
	iconfile.Append (DATADIR);
	iconfile.Append ("/icons/22x22/view-zoom-out.png");
	wxBitmap bitmap3 (iconfile);
	wxBitmapButton* zoomout = new wxBitmapButton (this, wxbID_ZOOM_OUT, bitmap3);
	zoomout->SetToolTip ("Zoom out");

	hbox1->Add (backbutton, 0, wxCENTRE|wxALL, 1);
	hbox1->Add (forwardbutton, 0, wxCENTRE|wxALL, 1);
	hbox1->Add (reloadbutton, 0, wxCENTRE|wxALL, 1);
	hbox1->Add (homebutton, 0, wxCENTRE|wxALL, 1);
	hbox1->Add (urltext, 1, wxCENTRE|wxALL, 1);
	hbox1->Add (addbookmarkbutton, 0, wxCENTRE|wxALL, 1);
	hbox1->Add (zoomin, 0, wxCENTRE|wxALL, 1);
	hbox1->Add (zoomout, 0, wxCENTRE|wxALL, 1);
	vbox1->Add (hbox1, 0, wxEXPAND | wxALL, 1);

	/* second row */
	wxBoxSizer *hbox2 = new wxBoxSizer (wxVERTICAL);

	webview = wxWebView::New (this, wxbID_WEBVIEW, url);
	urltext->ChangeValue (url);
	hbox2->Add (webview, 1, wxEXPAND|wxALL, 1);
	vbox1->Add (hbox2, 1, wxEXPAND | wxALL, 1);

	/* third row */
	/* search element */
	wxBoxSizer* searchbox = new wxBoxSizer (wxHORIZONTAL);
	m_closebutton = new wxBitmapButton (this, wxbID_SEARCH_BUTTON, wxArtProvider::GetBitmap (wxART_CLOSE));
	m_searchctrl = new wxSearchCtrl (this, wxbID_SEARCH_CTRL, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
	m_searchctrl->ShowCancelButton (true);
	m_searchtoggle = new wxToggleButton (this, wxbID_SEARCH_TOGGLE, "case sensitivity");

	searchbox->Add (m_closebutton, 0, wxLEFT, 0);
	searchbox->Add (m_searchctrl, 1, wxLEFT|wxEXPAND, 5);
	searchbox->Add (m_searchtoggle, 0, wxLEFT|wxEXPAND, 5);

	vbox1->Add (searchbox, 0, wxEXPAND, 1);

	SetSizer (vbox1);

	Connect (wxbID_BACKBUTTON, wxEVT_BUTTON, (wxObjectEventFunction) &WxbTabitem::OnBackButtonClicked);
	Connect (wxbID_FORWARDBUTTON, wxEVT_BUTTON, (wxObjectEventFunction) &WxbTabitem::OnForwardButtonClicked);
	Connect (wxbID_RELOADBUTTON, wxEVT_BUTTON, (wxObjectEventFunction) &WxbTabitem::OnReloadButtonClicked);
	Connect (wxbID_HOMEBUTTON, wxEVT_BUTTON, (wxObjectEventFunction) &WxbTabitem::OnHomeButtonClicked);
	Connect (wxbID_TEXTCTRL, wxEVT_TEXT_ENTER, (wxObjectEventFunction) &WxbTabitem::OnTextEnter);
	urltext->Connect (wxbID_TEXTCTRL, wxEVT_LEFT_DCLICK, wxCommandEventHandler (WxbTabitem::OnTextDoubleClick), NULL, this);
	Connect (wxbID_ADD_BOOKMARK, wxEVT_BUTTON, (wxObjectEventFunction) &WxbTabitem::OnAddBookmark);
	Connect (wxbID_ZOOM_IN, wxEVT_BUTTON, (wxObjectEventFunction) &WxbTabitem::OnZoomIn);
	Connect (wxbID_ZOOM_OUT, wxEVT_BUTTON, (wxObjectEventFunction) &WxbTabitem::OnZoomOut);

	Connect (wxbID_WEBVIEW, wxEVT_WEBVIEW_NAVIGATING, wxWebViewEventHandler (WxbTabitem::OnWebviewNavigating), NULL, this);
	Connect (wxbID_WEBVIEW, wxEVT_WEBVIEW_NAVIGATED, wxWebViewEventHandler (WxbTabitem::OnWebviewNavigated), NULL, this);
	Connect (wxbID_WEBVIEW, wxEVT_WEBVIEW_LOADED, wxWebViewEventHandler (WxbTabitem::OnWebviewLoaded), NULL, this);
	Connect (wxbID_WEBVIEW, wxEVT_WEBVIEW_ERROR, wxWebViewEventHandler (WxbTabitem::OnWebviewError), NULL, this);
	Connect (wxbID_WEBVIEW, wxEVT_WEBVIEW_NEWWINDOW, wxWebViewEventHandler (WxbTabitem::OnWebviewNewwindow), NULL, parent);
	Connect (wxbID_WEBVIEW, wxEVT_WEBVIEW_TITLE_CHANGED, wxWebViewEventHandler (WxbTabitem::OnWebviewTitlechanged), NULL, parent);

	Connect (wxbID_SEARCH_CTRL, wxEVT_SEARCHCTRL_SEARCH_BTN, (wxObjectEventFunction) &WxbTabitem::OnSearchCtrlSearch);
}

void WxbTabitem::OnBackButtonClicked (wxCommandEvent& event)
{
	webview->GoBack ();
}

void WxbTabitem::OnForwardButtonClicked (wxCommandEvent& event)
{
	webview->GoForward ();
}

void WxbTabitem::OnReloadButtonClicked (wxCommandEvent& event)
{
	switch (e_webview_status)
	{
		case IDLE:
			webview->Reload ();
		break;
		case LOADING:
			webview->Stop ();
		break;
	};
}

void WxbTabitem::OnHomeButtonClicked (wxCommandEvent& event)
{
	urltext->ChangeValue (m_startpage);
	webview->LoadURL (urltext->GetValue ());
}

void WxbTabitem::OnTextEnter (wxCommandEvent& event)
{
	wxRegEx regurl ("^https?://");
	wxString url;
	url = urltext->GetValue();
	if (!regurl.Matches (url))
	{
		url = "http://" + url;
	}
	webview->LoadURL (url);
}

void WxbTabitem::OnTextDoubleClick (wxCommandEvent& event)
{
	urltext->SetFocus ();
	urltext->SetSelection (-1, -1);
}

void WxbTabitem::OnAddBookmark (wxCommandEvent& event)
{
	WxbBookmarkDialog* pref = new WxbBookmarkDialog (this, _("Add bookmark"));
	pref->SetBookmark (webview->GetCurrentTitle (), webview->GetCurrentURL (), webview->GetPageText ());
	if (pref->ShowModal () == wxID_OK)
	{
		pref->SaveBookmark ();
	}
	pref->Destroy ();
	delete pref;
}

void WxbTabitem::OnZoomIn (wxCommandEvent& event)
{
	wxWebViewZoom zoom;
	
	webview->SetZoomType (wxWEBVIEW_ZOOM_TYPE_LAYOUT);
	zoom = webview->GetZoom ();
	switch (zoom)
	{
		case wxWEBVIEW_ZOOM_TINY:
			webview->SetZoom (wxWEBVIEW_ZOOM_SMALL);
		break;
		case wxWEBVIEW_ZOOM_SMALL:
			webview->SetZoom (wxWEBVIEW_ZOOM_MEDIUM);
		break;
		case wxWEBVIEW_ZOOM_MEDIUM:
			webview->SetZoom (wxWEBVIEW_ZOOM_LARGE);
		break;
		case wxWEBVIEW_ZOOM_LARGE:
			webview->SetZoom (wxWEBVIEW_ZOOM_LARGEST);
		break;
		case wxWEBVIEW_ZOOM_LARGEST:
			webview->SetZoom (wxWEBVIEW_ZOOM_LARGEST);
		break;
	};
}

void WxbTabitem::OnZoomOut (wxCommandEvent& event)
{
	wxWebViewZoom zoom;
	
	webview->SetZoomType (wxWEBVIEW_ZOOM_TYPE_LAYOUT);
	zoom = webview->GetZoom ();
	switch (zoom)
	{
		case wxWEBVIEW_ZOOM_TINY:
			webview->SetZoom (wxWEBVIEW_ZOOM_TINY);
		break;
		case wxWEBVIEW_ZOOM_SMALL:
			webview->SetZoom (wxWEBVIEW_ZOOM_TINY);
		break;
		case wxWEBVIEW_ZOOM_MEDIUM:
			webview->SetZoom (wxWEBVIEW_ZOOM_SMALL);
		break;
		case wxWEBVIEW_ZOOM_LARGE:
			webview->SetZoom (wxWEBVIEW_ZOOM_MEDIUM);
		break;
		case wxWEBVIEW_ZOOM_LARGEST:
			webview->SetZoom (wxWEBVIEW_ZOOM_LARGE);
		break;
	};
}

void WxbTabitem::OnWebviewNavigating (wxWebViewEvent& event)
{
	wxBitmap bitmap (wxArtProvider::GetBitmap (wxART_CROSS_MARK));
	reloadbutton->SetBitmap (bitmap);
	e_webview_status = LOADING;
}

void WxbTabitem::OnWebviewNavigated (wxWebViewEvent& event)
{
	urltext->SetValue (event.GetURL());
}

void WxbTabitem::OnWebviewLoaded (wxWebViewEvent& event)
{
	wxString iconfile (DATADIR);
	iconfile.Append ("/icons/22x22/view-refresh.png");
	wxBitmap bitmap (iconfile);
	reloadbutton->SetBitmap (bitmap);
	e_webview_status = IDLE;
}

void WxbTabitem::OnWebviewError (wxWebViewEvent& event)
{
	// wxLogMessage ("%s", "Error; url='" + event.GetURL() + "', error=' (" + event.GetString() + ")'");
	// std::cout << "Error: " << event.GetString () << std::endl;
}

void WxbTabitem::OnWebviewNewwindow (wxWebViewEvent& event)
{
	wxCommandEvent f_event (WXBEVT_NEWTAB, GetId());

	f_event.SetEventObject (this);
	f_event.SetString (event.GetURL ());
	ProcessWindowEvent (f_event);
}

void WxbTabitem::OnWebviewTitlechanged (wxWebViewEvent& event)
{
	wxCommandEvent evt (wxEVT_NOTEBOOK_PAGE_CHANGED, 1001);

	evt.SetEventObject (this);
	evt.SetString (event.GetString ().c_str());
	ProcessWindowEvent (evt);
}

void WxbTabitem::OnSearchCtrlSearch (wxWebViewEvent& event)
{
	wxWebViewFindFlags f_flags;

	if (m_searchtoggle->GetValue ())
	{
		f_flags = (wxWebViewFindFlags) (wxWEBVIEW_FIND_WRAP|wxWEBVIEW_FIND_HIGHLIGHT_RESULT|wxWEBVIEW_FIND_MATCH_CASE);
	}
	else
	{
		f_flags = (wxWebViewFindFlags) (wxWEBVIEW_FIND_WRAP|wxWEBVIEW_FIND_HIGHLIGHT_RESULT);
	}
	webview->Find (event.GetString (), f_flags);
}
