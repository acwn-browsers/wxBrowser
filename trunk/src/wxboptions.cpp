/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "wxboptions.h"

WxbOptions::WxbOptions (void)
{
	wxString f_dir;
	wxString f_file;

	f_dir = wxGetHomeDir ();
	f_dir.Append (wxT ("/.wxBrowser/"));
	if (!wxDir ().Exists (f_dir))
	{
		wxMkdir (f_dir, 0770);
	}
	f_file.Append (f_dir).Append (wxT ("config.cfg"));
	fconfig = new wxFileConfig (wxEmptyString, wxEmptyString, f_file,wxEmptyString, wxCONFIG_USE_LOCAL_FILE);
	m_options.startpage = fconfig->Read (wxT ("/General/startpage"), wxT ("about:blank"));
	m_options.savesize  = fconfig->Read (wxT ("/General/savesize"), true);
	m_options.height    = fconfig->Read (wxT ("/General/height"), 600);
	m_options.width     = fconfig->Read (wxT ("/General/width"), 800);
}

WxbOptions::~WxbOptions ()
{
	delete fconfig;
}

struct s_options WxbOptions::GetSettings (void) const
{
	return m_options;
}

void WxbOptions::SetSettings (struct s_options f_options)
{
	m_options = f_options;
	fconfig->Write (wxT ("/General/startpage"), f_options.startpage);
	fconfig->Write (wxT ("/General/savesize"), f_options.savesize);
	fconfig->Write (wxT ("/General/height"), f_options.height);
	fconfig->Write (wxT ("/General/width"), f_options.width);
}

int WxbOptions::FetchIntSetting (const char* key)
{
	wxString f_tmp (key, wxConvUTF8);
	return fconfig->Read (f_tmp, 0l);
}

wxString WxbOptions::FetchStringSetting (const char* key)
{
	wxString f_tmp (key, wxConvUTF8);
	return fconfig->Read (f_tmp, wxEmptyString);
}

std::string WxbOptions::FetchStdStringSetting (const char* key)
{
	wxString f_tmp (key, wxConvUTF8);
	wxString f_value;
	f_value = fconfig->Read (f_tmp, wxEmptyString);
	return std::string (f_value.mb_str ());
}
