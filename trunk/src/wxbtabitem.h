/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBTABITEM__
#define __WXBTABITEM__

#include <wx/wx.h>
#include <wx/webview.h>
#include <wx/srchctrl.h>
#include <wx/tglbtn.h>

class WxbTabitem : public wxPanel
{
	public:
		WxbTabitem (wxWindow* parent, wxString url, wxString startpage);
	private:
		wxBitmapButton* reloadbutton;
		wxTextCtrl* urltext;
		wxWebView* webview;
		wxBitmapButton* m_closebutton;
		wxSearchCtrl *m_searchctrl;
		wxToggleButton *m_searchtoggle;
		wxString m_url;
		wxString m_startpage;

		enum
		{
			wxbID_TEXTCTRL = 1000,
			wxbID_BACKBUTTON = 1001,
			wxbID_FORWARDBUTTON = 1002,
			wxbID_RELOADBUTTON = 1003,
			wxbID_HOMEBUTTON = 1004,
			wxbID_WEBVIEW = 1005,
			wxbID_SEARCH_BUTTON = 1006,
			wxbID_SEARCH_CTRL = 1007,
			wxbID_SEARCH_TOGGLE = 1008,
			wxbID_ADD_BOOKMARK,
			wxbID_ZOOM_IN,
			wxbID_ZOOM_OUT
		};
		
		enum
		{
			LOADING,
			IDLE
		} e_webview_status;
		
		void OnBackButtonClicked (wxCommandEvent& event);
		void OnForwardButtonClicked (wxCommandEvent& event);
		void OnReloadButtonClicked (wxCommandEvent& event);
		void OnHomeButtonClicked (wxCommandEvent& event);
		void OnTextEnter (wxCommandEvent& event);
		void OnTextDoubleClick (wxCommandEvent& event);
		void OnAddBookmark (wxCommandEvent& event);
		void OnZoomIn (wxCommandEvent& event);
		void OnZoomOut (wxCommandEvent& event);

		/* Webview event callbacks */
		void OnWebviewNavigating (wxWebViewEvent& event);
		void OnWebviewNavigated (wxWebViewEvent& event);
		void OnWebviewLoaded (wxWebViewEvent& event);
		void OnWebviewError (wxWebViewEvent& event);
		void OnWebviewNewwindow (wxWebViewEvent& event);
		void OnWebviewTitlechanged (wxWebViewEvent& event);
		
		/* Search button */
		void OnSearchCtrlSearch (wxWebViewEvent& event);
		
};

#endif
