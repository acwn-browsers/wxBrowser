/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBTABBOOK__
#define __WXBTABBOOK__

#include <wx/wx.h>
#include <wx/notebook.h>
#include <wx/webview.h>

class WxbTabbook : public wxNotebook 
{
	public:
		WxbTabbook (wxWindow *parent, wxWindowID id, wxString startpage);
		void AddTab (wxString url);
		void AddNewTab (wxCommandEvent& event);
		void CloseTab (void);
		void ChangedTabTitle (wxBookCtrlEvent& event);
		void ChangingTabTitle (wxBookCtrlEvent& event);
		void UpdateStartpage (wxString url);
	private:
		wxString m_startpage;
};

#endif
