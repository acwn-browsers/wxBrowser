/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBEVENTS__
#define __WXBEVENTS__

/* DEFINE in wxbtabbook.cpp */
wxDECLARE_EVENT(WXBEVT_NEWTAB, wxCommandEvent);

/* DEFINE in wxbframe.cpp */
wxDECLARE_EVENT(WXBEVT_SETMAINTITLE, wxCommandEvent);

#endif
