/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBBOOKMARKDIALOG__
#define __WXBBOOKMARKDIALOG__

#include <wx/wx.h>

#include "wxbbookmarks.h"

class WxbBookmarkDialog : public wxDialog
{
	private:
		WxbBookmarks *marks;
		enum
		{
			wxB_NAME = 2001,
			wxB_ADDRESS,
			wxB_CATEGORY,
			wxB_DESCRIPTION
		};
		wxTextCtrl *m_name;
		wxTextCtrl *m_address;
		wxComboBox *m_category;
		wxTextCtrl *m_description;
		void OnComboboxEnter (wxCommandEvent& event);
	public:
		WxbBookmarkDialog (wxWindow* parent, const wxString& title);
		~WxbBookmarkDialog ();
		void SetBookmark (wxString name, wxString address, wxString description);
		int SaveBookmark (void);
};

#endif
