/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBPREFERENCES__
#define __WXBPREFERENCES__

// #include <wx/wx.h>
#include <wx/propdlg.h>
#include <wx/bookctrl.h>
#include <wx/spinctrl.h>

#include "wxboptions.h"

class WxbPreferences : public wxPropertySheetDialog
{
	public:
		WxbPreferences (wxWindow* parent, const wxString& title);
		~WxbPreferences ();
		void SetSettings (struct s_options f_options);
		struct s_options GetSettings (void);
	private:
		enum
		{
			wxbID_STARTPAGE,
			wxbID_SIZEABLE,
			wxbID_HEIGHT,
			wxbID_WIDTH
		};
		/* widgets */
		wxTextCtrl* tc_startpage;
		wxCheckBox* checkbox1;
		wxStaticText* text21;
		wxStaticText* text22;
		wxSpinCtrl* spinner21;
		wxSpinCtrl* spinner22;

		/* functions */
		void onDisplayHelp (wxCommandEvent& event);
		wxPanel* CreateGeneralPage (void);
		void OnCheckBoxClicked (wxCommandEvent& event);
};

#endif
